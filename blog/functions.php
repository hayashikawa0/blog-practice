<?php
// 定数　
//define("DB_NAME", "blog");
const DB_NAME = "blog";
const DB_ACCOUNT = "root";

ini_set( 'display_errors', "On");

function v($data)
{
	echo "<pre>";
	var_dump($data);
	echo "</pre>";
}

function datetime_format($time)
{
	$date = new DateTime($time);
	return 	$date->format("Y年m月d日 H:i");
}

function connection_db()
{
	return new PDO("mysql:dbname=".DB_NAME, DB_ACCOUNT);
}
