<?php
require_once("functions.php");

$db = connection_db();

$error_notes = "";
$title = "";
if(isset($_POST["submit"]))
{
  $title = $_POST["title"];
  $content = $_POST["content"];
  move_uploaded_file($_FILES["image"]["tmp_name"], "upload.jpg");

  if(!$title)
  {
    $error_notes .= "タイトルが入力されていません<br>";
  }
  if(!$content)
  {
    $error_notes .= "本文が入力されていません";
  }

  if(!$error_notes)// エラーに何も入っていなければ送信する
  {
    // SQL文の実行
    $st = $db->query("INSERT INTO `post` (`id`, `title`, `content`, `time`) VALUES(NULL, '{$title}', '{$content}', current_timestamp());");
    header("Location: index.php"); // トップに戻る
  }
}
 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ブログ</title>
	<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body>

	<main class="blog">

		<form action="post.php" method="post" enctype="multipart/form-data">
			<section class="article">
				<h2 class="article__title">記事投稿</h2>

				<div class="article__post">
					<div class="article__postSet">
						<p>題名</p>
						<p><input type="text" name="title" value="<?php echo $title?>" size="40"></p>
					</div>

					<div class="article__postSet">
						<p>本文</p>
						<p><textarea name="content" row="8" cols="40"></textarea></p>
					</div>

          <div class="article__postSet">
            <p>画像</p>
            <p><input type="file" name="image"></p>
          </div>


					<div class="article__postSet">
						<p><input class="article__submit" name="submit" type="submit" value="投稿"></p>
		    		<p>
            <?php if($error_notes): ?>
            <?php echo $error_notes   ?>
          <?php endif; ?>
		    		</p>
		    	</div>
		    </div>

			</section>
      <a href="index.php">前に戻る</a>
		</form>
	</main>

</body>
</html>
