<?PHP /*
データベース設計について考える
どんなデータを記録しておいたら便利かを考える。

・記事用のテーブルを作る？
記事名、本文、日付、、、、他に必要なものがあるか？
idはつけよう！

id=1,　記事名=こんにちは, 本文=こんばんは, コメント=アロハ, コメント=ハロー
↑データベースでは簡単にカラム数を増やせないので、この設計には無理がある。

コメントはどうやら別のテーブルとして分けた方が良さそう。
commentテーブルを作成する。

id, 名前（name）, 本文（comment）, どの記事と紐づいているか（本記事のIDでよいのでは？）

nl2br();
改行
*/ ?>
<?php
require_once("functions.php");

$pdo = connection_db();
$st_post = $pdo->query("SELECT * FROM `post`");
$posts = $st_post->fetchAll();
//$postsの中にそれぞれの記事のコメントを配列で代入する（キーは「"comments"」）
for($i = 0; $i<count($posts); $i++)
{
	$id = $posts[$i]["id"];
	$pdo = connection_db();
	$st_comment = $pdo->query("SELECT * FROM `comment` WHERE `post_id` = $id");
	$comments = $st_comment->fetchAll(PDO::FETCH_ASSOC);
	$posts[$i]["comment"] = $comments;
}


include("./tmpl/header.tmpl");
include("./tmpl/top.tmpl");
include("./tmpl/footer.tmpl");

?>
