<?php

class file
{
// プロパティ
  private $conf;
  private $size;
  private $tmpl;

// メソッド
  // テンプレートの読み込み
  public function setTmpl($file,$type)
  {
    $this->conf = fopen("tmpl/{$file}.tmpl","{$type}") or die;
    $this->size = filesize("tmpl/{$file}.tmpl");
    $this->tmpl = fread($this->conf, $this->size);
    fclose($this->conf);
  }

  public function getTmpl()
  {
    return $this->tmpl;
  }
}
?>
