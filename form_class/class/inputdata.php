<?php

class inputData
{
// プロパティ
  private $name;
  private $comment;
  private $email;

// メソッド
  function __construct()
  {
    $this->name = $_POST["name"];
    $this->comment = $_POST["comment"];
    $this->email = $_POST["email"];
  }

  public function gName()
  {
    return $this->name;
  }
  public function gComment()
  {
    return $this->comment;
  }
  public function gEmail()
  {
    return $this->email;
  }
}

?>
