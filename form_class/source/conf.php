<?php
#-----------------------------------------------------------
#  確認画面
#-----------------------------------------------------------
function conf_form()
{
  $form = new inputData;
  $conf = new file;

  $name = $form->gName();
  $email = $form->gEmail();
  $comment = $form->gComment();

  # テンプレート読み込み
  $conf->setTmpl("conf","r");

  # 文字置き換え
  $data = $conf->getTmpl();

  $data = str_replace("!name!", $name, $data);
  $data = str_replace("!email!", $email, $data);
  $data = str_replace("!comment!", $comment, $data);

   # 表示
   echo $data;
   exit;
}
?>
