<?php
require_once "../class/file.php";

#-----------------------------------------------------------
#  エラー画面
#-----------------------------------------------------------
function displayError($name, $email, $comment)
{
  # 入力チェック
  $error_text = "";
  if ($name == "")
  {
    $error_text .= "・名前が未入力です<br>";
  }
  if (!preg_match("/\w+@\w+/",$email))
  {
    $error_text .= "・メールアドレスが不正です<br>";
  }
  if ($comment == "")
  {
    $error_text .= "・コメントが未入力です<br>";
  }

  if($error_text != "")
  {
    error($error_text);
  }
}

function error($msg)
{
  $error = new file;

  # テンプレート読み込み
  $error->setTmpl("error","r");

  #文字置き換え
  $data = $error->getTmpl();
  $data = str_replace("!message!", $msg, $data);

  #表示
  echo $data;
  exit;
}
?>
