<?php
const DB_NAME = "form";
const DB_ACCOUNT = "root";

function connection_db()
{
	return new PDO("mysql:dbname=".DB_NAME, DB_ACCOUNT);
}

// データ型の確認
function v($data)
{
  echo "<pre>";
  echo var_dump($data);
  echo "</pre>";
}

function datetime_format($time)
{
	$date = new DateTime($time);
	return 	$date->format("Y年m月d日 H:i");
}
