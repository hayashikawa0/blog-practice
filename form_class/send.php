<?php

#-----------------------------------------------------------
#  CSV書込
#-----------------------------------------------------------
function send_form(){
  $form = new inputData;
  $send = new file;

  $name = $form->gName();
  $email = $form->gEmail();
  $comment = $form->gComment();
  //$pdo = connection_db();
  //$st = $pdo->query("INSERT INTO `post` (`id`, `name`, `e_mail`, `comment`, `time`)
  //VALUES (NULL, 'hayashi', 'bb@aa', 'テスト', current_timestamp())");

  $user_input = array($name,$email,$comment);
  mb_convert_variables("SJIS","UTF-8",$user_input);
  $fh = fopen("data/user.csv","a");
  flock($fh,LOCK_EX);
  fputcsv($fh, $user_input);
  flock($fh,LOCK_UN);
  fclose($fh);

  # テンプレート読み込み
  $send->setTmpl("send","r");
  #文字置き換え
  $data = $send->getTmpl();
  global $toppage;
  $data = str_replace("!top!", $toppage, $data);
  #表示
  echo $data;
  exit;
}

?>
