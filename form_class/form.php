﻿<?php
// エラー表示
ini_set('display_error',"On");

require_once("./class/inputdata.php");
require_once("./class/file.php");
require_once("functions.php");

# 送信後画面からの戻り先
$toppage = "./form.html";

#===========================================================
#  入力情報の受け取りと加工
#===========================================================
  require_once("error.php");
  require_once("conf.php");
  require_once("send.php");

  $form = new inputData;

  $name = $form->gName();
  $email = $form->gEmail();
  $comment = $form->gComment();

  # 無効化
  $name  = htmlentities($name,ENT_QUOTES, "UTF-8");
  $email = htmlentities($email,ENT_QUOTES, "UTF-8");
  $comment = htmlentities($comment,ENT_QUOTES, "UTF-8");

  # 改行処理
  $name = str_replace("\r\n", "", $name);
  $email = str_replace("\r\n", "", $email);
  $comment = str_replace("\r\n", "\t", $comment);
  $comment = str_replace("\r", "\t", $comment);
  $comment = str_replace("\n", "\t", $comment);

  displayError($name, $email, $comment);

  # 分岐チェック
  if ($_POST["mode"] == "post")
  {
    conf_form();
  }
  else if($_POST["mode"] == "send")
  {
    send_form();
  }

?>
